<?php

class Nullor_Bing_Block_Code extends Mage_Core_Block_Template {

    private function _getSection(){
        $pageSection  = Mage::app()->getFrontController()->getAction()->getFullActionName(); 
        return  $pageSection; 
    }


	/**
	 * Renders pixel code if module is enabled
	 */
	public function _toHtml()
    {
        if (Mage::helper('nullor_bing')->isEnabled()){
            return parent::_toHtml();
        }
    }

    public function bingCode(){
        $_code = '<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5739021"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5739021&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>';
        $pageSection        = $this->_getSection();
        
        if(Mage::helper('nullor_bing')->isEnabled()){
            /**
             * x.com/customer/account/create/
             * x.com/checkout/cart/
             * x.com/checkout/onepage/success/
             */
            switch($pageSection){
                case 'customer_account_create':
                case 'checkout_cart_index':
                case 'checkout_onepage_success':
                //case 'checkout_multishipping_success':
                //case 'checkout_onepage_failure':
                    return $_code;
                    break;
                default:
                    return '';
            }
        }  
        return '';
    }
}