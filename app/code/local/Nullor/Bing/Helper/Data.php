<?php 


class Nullor_Bing_Helper_Data extends Mage_Core_Helper_Abstract
{

    //Config paths
    const MODULE_ENABLED            = 'nullor_bing/general/enabled';
    //const DEBUG                     = 'nullor_bing/general/debug';
 

	public function isEnabled($store = null)
    {
        return Mage::getStoreConfig(self::MODULE_ENABLED, $store);
    }

    /*
    public function isDebug($store = null)
    {
        return $this->isEnabled($store) && Mage::getStoreConfig(self::DEBUG, $store);
    }
    */

}